Calculus Plots
======

Exercise to draw the Calculus plots

Project Information
-------------------

Draw the plots for:
 * polynormial
 * derivatives
 * points of inflection
 * integral of a polynormial

Build Prerequisites
===================
* IntelliJ
* nodejs
* mathjs
* plotly
* integraljs


How to build the plots
=============

To use node js as the http web server:
 - cd ./calculator/web/math
 - node node_http_server.js

Then you can check the exercise of the plots:
 - http://localhost:9000/Part_1.html
 - http://localhost:9000/Part_2.html
 - http://localhost:9000/Part_3.html
 - http://localhost:9000/Part_5.html
