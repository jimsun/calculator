const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
const port = process.argv[2] || 9000;

http.createServer(function (req, res) {
    console.log(`${req.method} ${req.url}`);
    const serverPath = process.cwd()
    console.log("The server directory: " + serverPath)
    // parse URL
    const parsedUrl = url.parse(req.url);

    // extract URL path
    let pathname = `.${parsedUrl.pathname}`;
    console.log("pathname=" + pathname)

    // extract the name part of the requested xxx.html file
    const fileNameOnly = path.basename(pathname)
    console.log("fileNameOnly=" + fileNameOnly)

    // based on the URL path, extract the file extention. e.g. .js, .doc, ...
    const ext = path.parse(pathname).ext;
    console.log("ext=" + ext)

    // maps file extention to MIME typere
    const map = {
        '.ico': 'image/x-icon',
        '.html': 'text/html',
        '.js': 'text/javascript',
        '.json': 'application/json',
        '.css': 'text/css',
        '.png': 'image/png',
        '.jpg': 'image/jpeg',
        '.wav': 'audio/wav',
        '.mp3': 'audio/mpeg',
        '.svg': 'image/svg+xml',
        '.pdf': 'application/pdf',
        '.doc': 'application/msword'
    };

    let absoluteFilePath = path.join(serverPath, fileNameOnly);
    console.log("absoluteFilePath=" + absoluteFilePath)

    fs.exists(absoluteFilePath, function (exist) {
        if (!exist) {
            // if the file is not found, return 404
            res.statusCode = 404;
            res.end(`File ${absoluteFilePath} not found!`);
            return;
        }

        // if is a directory search for index file matching the extention
        if (fs.statSync(absoluteFilePath).isDirectory()) absoluteFilePath += '/index' + ext;

        // read file from file system
        fs.readFile(absoluteFilePath, function (err, data) {
            if (err) {
                res.statusCode = 500;
                res.end(`Error getting the file: ${err}.`);
            } else {
                // if the file is found, set Content-type and send data
                res.setHeader('Content-type', map[ext] || 'text/plain');
                res.end(data);
            }
        });
    });


}).listen(parseInt(port));

console.log(`Server listening on port ${port}`);