//https://plot.ly/nodejs/

const math = require('mathjs')
math.import(require('mathjs-simple-integral'));
var plotly = require('plotly')("saltsurge", "OeQFGvmSE9hURAzDa0al")

// examples to call the math.integral function
// var theIntegral = math.integral('x^2', 'x', {simplify: false});
// console.log("the Integral of 'x^2' is: " + theIntegral.toString())
// console.log(math.integral('x^2', 'x')); // 'x ^ 3 / 3'
// math.integral('1/x', 'x'); // 'log(abs(x))'
// math.integral('e^x', 'x'); // 'e^x'
// math.integral('cos(2*x+pi/6)', 'x'); // 'sin(2 * x + pi / 6) / 2'

var polynormialString = "4 * sin(x) + 5 * cos(x/2)"
console.log("polynormial function is: " + polynormialString)
const polynormialExpr = math.compile(polynormialString)

// evaluate the polynormialString repeatedly for different values of x
const xValues = math.range(-10, 10, 0.5).toArray()
const yValues = xValues.map(function (x) {
    return polynormialExpr.eval({x: x})
})

// This is the trace for the polynormial function
const polynormialTrace = {
    x: xValues,
    y: yValues,
    type: 'scatter',
    name: 'Polynormial',
    mode: 'lines+markers',
}

// Let's find the integral function for the polynormial
var integralString = math.integral(polynormialString, 'x', {simplify: false});
console.log("integral function is: " + integralString.toString())
const exprIntegral = math.compile(integralString.toString())
const yValuesIntegral = xValues.map(function (x) {
    return exprIntegral.eval({x: x})
})

// This is the trace for the integral function
const integralTrace = {
    x: xValues,
    y: yValuesIntegral,
    type: 'scatter',
    name: 'Integral',
}

// The two x values for the start and end point of the integral
const firstX = 2
const secondX = 5

var firstYValue = [firstX].map(function (x) {
    return exprIntegral.eval({x: x})
})
console.log("firstYValue[0]=" + firstYValue[0])

var secondYValue = [secondX].map(function (x) {
    return exprIntegral.eval({x: x})
})
console.log("secondYValue[0]=" + secondYValue[0])

// find the start line for the integral
// the points of inflection needs to be calculated using the 2nd derivative of a polynormial
function getIntegralPoints(integralPoint) {
    var integralPoints = []
    for (y = 0; y <= integralPoint; y += 0.5) {
        integralPoints.push(y)
    }
    return integralPoints
}

const integralYValues1 = getIntegralPoints(firstYValue[0])
console.log('max of y points: ' + math.max(integralYValues1))
const integralXValues1 = xValues.map(function (x) {
    return firstX
})
const integralLine1 = {
    x: integralXValues1,
    y: integralYValues1,
    type: 'scatter',
    name: 'startX',
    mode: 'lines',
    text: 'startX',
    title: 'startX',
}

// now find the finish line for the integral
var integralYValues2 = getIntegralPoints(secondYValue[0])
console.log('max of y points: ' + math.max(integralYValues2))
const integralXValues2 = xValues.map(function (x) {
    return secondX
})
const integralLine2 = {
    x: integralXValues2,
    y: integralYValues2,
    type: 'scatter',
    name: 'endX',
    mode: 'lines',
    text: 'startX',
    title: 'startX',
}

// There will be 4 lines on plot
const data = [polynormialTrace, integralTrace, integralLine1, integralLine2]

// The visual part of the plot
var layout = {
    title: "Fundamental Theorem of Calculus",
    xaxis: {
        autotick: false,
        ticks: "outside",
        tick0: 0,
        dtick: 0.5,
        ticklen: 8,
        tickwidth: 4,
        tickcolor: "#000",
        title: 'x value'
    },
    yaxis: {
        autotick: false,
        ticks: "outside",
        tick0: 0,
        dtick: 0.5,
        ticklen: 8,
        tickwidth: 4,
        tickcolor: "#000",
        title: 'y value'
    }
};
var graphOptions = {layout: layout, filename: "integral", fileopt: "overwrite"};

plotly.plot(data, graphOptions, function (err, msg) {
    if (err) return console.log(err);
    console.log(msg);
});