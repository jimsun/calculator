1. Install node.js
2. Install the npm packages
 npm install mathjs
 npm install mathjs-simple-integral
 npm install plotly

3. Run the node code:

> node Part_4.js

APPLECHEAPLIMIT:node jim.sun$ node Part_4.js
polynormial function is: 4 * sin(x) + 5 * cos(x/2)
integral function is: 4 * -cos(x) + 5 * sin(x * 2 ^ -1) / (1 * 2 ^ -1)
firstYValue[0]=10.079297194267536
secondYValue[0]=4.85007269918666
max of y points: 10
max of y points: 4.5
{ streamstatus: undefined,
  url: 'https://plot.ly/~saltsurge/0',
  message:
   'High five! You successfully sent some data to your account on plotly. View your plot in your browser at https://plot.ly/~saltsurge/0 or inside your plot.ly account where it is named \'integral\'',
  warning: '',
  filename: 'integral',
  error: '' }


To use node js as the http web server:
cd ./calculator/web/math
node node_http_server.js

Then you can check the exercise of the plots:
http://localhost:9000/Part_1.html
http://localhost:9000/Part_2.html
http://localhost:9000/Part_3.html
http://localhost:9000/Part_4.html